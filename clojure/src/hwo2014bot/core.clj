(ns hwo2014bot.core
  (:require [clojure.data.json :as json])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
    (tcp-client {:host  host,
                 :port  port,
                 :frame (string :utf-8 :delimiters ["\n"])})))

(def cur-car-positions (atom {}))

(def my-car (atom {}))

(defn update-my-car-info! [data]
  (reset! my-car data))

(defn update-car-positions! [data]
  (reset! cur-car-positions data))

(def race-info (atom {
                       :track       nil
                       :cars        nil
                       :raceSession nil
                       }))

(defn update-race-info! [msg]
  (reset! race-info (-> msg :data :race)))

(defn get-race-pieces []
  (-> @race-info
      :track
      :pieces))

(defn get-my-car-position [car-positions]
  (first (filter #(= (-> % :id :name) (:name @my-car)) car-positions)))

(defn get-piece-type [piece]
  (cond
    (and (get piece :length) (get piece :switch)) :switch
    (get piece :length) :straignt
    :else :curve))

(defn get-next-piece [index]
  (let [pieces (get-race-pieces)]
    (if (< index (dec (count pieces)))
      (get pieces (inc index))
      (get pieces 0))))

(defn near-curve?
  "前面是否靠近弯道(这时候需要减速,否则加速)"
  [car-positions]
  (let [my-car-postion (get-my-car-position car-positions)
        angle (:angle my-car-postion)
        piece-postion (:piecePosition my-car-postion)
        piece-index (:pieceIndex piece-postion)
        in-piece-distance (:inPieceDistance piece-postion)
        pieces (get-race-pieces)
        piece (get pieces piece-index)
        next-piece (get-next-piece piece-index)]
    (if (= (get-piece-type piece) :curve)
      (do
        (println "curve angle" angle)
        :curve)
      (if (= (get-piece-type piece) :switch)
        (do
          (println "switch angle" angle)
          :switch)
        (let [cur-piece-len (:length piece)
              rem-piece-len (- cur-piece-len in-piece-distance)]
          (if (= (get-piece-type next-piece) :straignt)
            false
            (if (< rem-piece-len 30)
              (get-piece-type next-piece)
              false)))))))

(defmulti handle-msg :msgType)

(defmethod handle-msg "carPositions" [msg]
  ;; TODO: 根据角度不同得到不同的速度,角度越大速度越慢
  ;; TODO: 判断是否堵在了一个角上停下来了(头部来回打转)
  (case (near-curve? @cur-car-positions)
    :curve {:msgType "throttle" :data 0.541}
    :switch {:msgType "throttle" :data 0.561}
    {:msgType "throttle" :data 0.77}))

(defmethod handle-msg :default [msg]
  {:msgType "ping" :data "ping"})

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "yourCar" (do (println msg) (update-my-car-info! (:data msg)))
    "gameStart" (println "Race started")
    "gameInit" (update-race-info! msg)
    "carPositions" (update-car-positions! (:data msg))
    "crash" (do
              (println "Someone crashed and current position is " @cur-car-positions)
              (println "track is " (:track @race-info)))
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    "finish" (println msg)
    :noop))

(defn game-loop [channel]
  (let [msg (read-message channel)]
    (log-msg msg)
    (send-message channel (handle-msg msg))
    (recur channel)))

(defn -main [& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel {:msgType "join" :data {:name botname :key botkey}})
    (game-loop channel)))
